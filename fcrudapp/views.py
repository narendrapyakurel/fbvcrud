from django.shortcuts import render,redirect,get_object_or_404
from .models import Book
from .forms import BookForm

def index(request):
	template='list.html'
	book=Book.objects.all()
	context={'book':book}
	return render(request,template,context)

def book_create(request):
	template='form.html'
	form=BookForm(request.POST or None)
	if form.is_valid():
		form.save()
		return redirect('fcrudapp:home')

	context={"form":form}
	return render(request,template,context)

def book_update(request,pk):
	template='form.html'
	book=get_object_or_404(Book,pk=pk)
	form=BookForm(request.POST or None,instance=book)
	if form.is_valid():
		form.save()
		return redirect('fcrudapp:home')
	context={"form":form}
	return render(request,template,context)
def book_delete(request,pk):
	template='delete.html'
	book=get_object_or_404(Book,pk=pk)
	if request.method == 'POST':
		book.delete()
		return redirect('fcrudapp:home')
	context={'book':book}
	return render(request,template,context)













